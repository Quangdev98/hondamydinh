
// mm-wrapper_opened mm-wrapper_blocking mm-wrapper_background mm-wrapper_opening
$(document).ready(function () {
	$('#menu').mmenu();

	$('.mm-menu').append(`<div id="close-nav"><i class="fal fa-times"></i></div>`)
	$('#toggle-menu').click(function(){
		$('html').attr('class', 'mm-wrapper_opened mm-wrapper_blocking mm-wrapper_background mm-wrapper_opening');
		$('#menu').toggleClass('mm-menu_opened');
		$('#menu').removeAttr('aria-hidden')
	})
});
$(document).on('click', '#close-nav', function(){
	$('html').attr('class', '');
	$('#menu').toggleClass('mm-menu_opened');
	$('#menu').attr('aria-hidden', true);
})
$('#slide-top, .wrap-slide-bolg').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
		},
	}
});
$('#slide-item-product-detail').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayHoverPause: true,
	margin: 20,
	responsive: {
		0: {
			items: 2,
			margin: 10,
		},
		414: {
			items: 3
		},
		1024: {
			items: 4
		}
	}
});

$('#product-same').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayHoverPause: true,
	margin: 20,
	responsive: {
		0: {
			items: 1,
		},
		576: {
			items: 3
		},
		1024: {
			items: 4
		}
	}
});

// fix header
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#menu-box").css({
			"position": "sticky",
			"top": 0,
		});
	} else {
		$("#menu-box").css({
			"top": "-40px",
		});
	}
	prevScrollpos = currentScrollPos;
}


$(document).ready(function () {
	$("#myScrollspy a").on('click', function (event) {
		if (this.hash !== "") {
			event.preventDefault();

			var hash = this.hash;
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 800, function () {

				window.location.hash = hash;
			});
		}
	});
});



function setWidthText() {
	let showChar = 305;
	var ellipsestext = "...";
	var data = ['content-hot'];
	data.forEach(function (value) {

		$('.' + value).each(function () {
			var content = $(this).html();
			if (content.length > showChar) {
				var c = content.substr(0, showChar);
				var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp';
				$(this).html(html);
			}

		});
	});
}
setWidthText();

// count number 

var count = 1;
	$("#minus").click(function () {
		if (count > 1) {
			count--;
			$('#number-product').val(count);
			if($("#minus").hasClass('remove-minus')){
				$('#minus').removeClass('remove-minus')
			}
		} else {
			$('#minus').addClass('remove-minus')
		}
	});
	$("#plus").click(function () {
			count++;
			$('#number-product').val(count);
			$('#minus').removeClass('remove-minus')
	});

	
$('[data-toggle="tooltip"]').tooltip()


// function resizeImage() {
// 	let arrClass = [
// 		{ class: 'item-video-list', number: (332 / 555) },
// 	];
// 	for (let i = 0; i < arrClass.length; i++) {
// 		let width = $("." + arrClass[i]['class']).width();
// 		$("." + arrClass[i]['class']).css('width', width * arrClass[i]['number'] + 'px');
// 		console.log(width);
// 		// console.log(arrClass);
// 		// console.log(width*arrClass[i]['number']);
// 	}
// }
